FROM tomcat:7
# Take the war and copy to webapps of tomcatserver
COPY /target/myweb.war /usr/local/tomcat/webapps/
#COPY /var/lib/jenkins/workspace/devops/manager.xml /usr/local/tomcat/conf/
#COPY /var/lib/jenkins/workspace/devops/tomcat-users.xml /usr/local/tomcat/conf
